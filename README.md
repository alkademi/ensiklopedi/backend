# Ensiklopedi Server

## How to run

```cmd
go run main.go runserver --config=./config/config.yaml
```

## DOCS

After running, go to <http://localhost:8080/docs/index.html>

## How to generate DOCS

1. Install [swag](https://github.com/swaggo/swag)
2. ```swag init -o ./docs```
