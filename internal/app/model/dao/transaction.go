package dao

import (
	transaction_status "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao/trxStatus"
	"gorm.io/gorm"
)

type Transaction struct {
	gorm.Model

	UserID uint `gorm:"column:id_user"`
	User   User `gorm:"->;foreignKey:UserID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	BookID uint `gorm:"column:id_book"`
	Book   Book `gorm:"->;foreignKey:BookID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	Status transaction_status.TransactionStatus `gorm:"column:status"`
	Amount int                                  `gorm:"column:amount"`
}
