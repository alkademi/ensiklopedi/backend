package dao

import "gorm.io/gorm"

type Collection struct {
	gorm.Model

	UserID uint `gorm:"column:id_user"`
	User   User `gorm:"->;foreignKey:UserID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	BookID uint `gorm:"column:id_book"`
	Book   Book `gorm:"->;foreignKey:BookID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	Progress int `gorm:"default:0"`
}
