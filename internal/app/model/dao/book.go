package dao

import "gorm.io/gorm"

type Book struct {
	gorm.Model

	Title       string `gorm:"size:512"`
	Price       int    `gorm:"default:0;not null"`
	Author      string `gorm:"size:255"`
	Description string
	Publisher   string `gorm:"size:255"`
	ISBN        string `gorm:"size:50"`
	Cover       string `gorm:"size:255"`
	Content     string `gorm:"size:255"`
}
