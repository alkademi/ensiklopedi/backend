package dao

import "gorm.io/gorm"

type Member struct {
	gorm.Model

	BookID uint `gorm:"column:id_book"`
	Book   Book `gorm:"->;foreignKey:BookID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`

	CategoryID uint     `gorm:"column:id_category"`
	Category   Category `gorm:"->;foreignKey:CategoryID;references:ID;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
}
