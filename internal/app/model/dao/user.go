package dao

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Name     string  `gorm:"not null;size:255"`
	Email    string  `gorm:"unique;not null"`
	Password string  `gorm:"not null;size:255"`
	Balance  float64 `gorm:"check:balance >= 0"`
	IsAdmin  bool    `gorm:"default:0"`
}
