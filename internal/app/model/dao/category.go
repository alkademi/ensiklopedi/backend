package dao

import "gorm.io/gorm"

type Category struct {
	gorm.Model
	CategoryName string `gorm:"not null;size:255;unique"`
}
