package dto

import (
	"time"

	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
)

type (
	CreateCollectionRequestDTO struct {
		UserID uint `json:"id_user" validate:"required"`
		BookID uint `json:"id_book" validate:"required"`
	}

	CollectionDTO struct {
		ID       uint              `json:"id"`
		BookID   uint              `json:"id_book"`
		Progress int               `json:"progress"`
		Book     CollectionBookDTO `json:"book"`
	}

	MyCollectionDTO struct {
		UserID      uint            `json:"id_user"`
		BookRead    int             `json:"book_read"`
		Collections []CollectionDTO `json:"collections"`
	}

	ProgressDTO struct {
		ID        uint      `json:"id"`
		BookID    uint      `json:"id_book"`
		Progress  int       `json:"progress"`
		UpdatedAt time.Time `json:"updated_at"`
	}
)

func (c CreateCollectionRequestDTO) ToDAO() dao.Collection {
	return dao.Collection{}
}

func NewCollectionDTO(collection dao.Collection) CollectionDTO {
	return CollectionDTO{
		ID:       collection.ID,
		BookID:   collection.BookID,
		Progress: collection.Progress,
		Book:     NewCollectionBookDTO(collection.Book),
	}
}

func NewMyCollectionDTO(collections []dao.Collection, userID uint) MyCollectionDTO {
	id_user := userID
	my_collection := []CollectionDTO{}
	bookRead := 0

	for _, collection := range collections {
		if collection.Progress > 0 {
			bookRead += 1
		}
		my_collection = append(my_collection, NewCollectionDTO(collection))
	}

	return MyCollectionDTO{
		UserID:      id_user,
		BookRead:    bookRead,
		Collections: my_collection,
	}
}

func NewProgressDTO(collection dao.Collection) ProgressDTO {
	return ProgressDTO{
		ID:        collection.ID,
		BookID:    collection.BookID,
		Progress:  collection.Progress,
		UpdatedAt: collection.UpdatedAt,
	}
}
