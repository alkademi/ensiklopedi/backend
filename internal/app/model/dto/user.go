package dto

import "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"

type (
	CreateUserRequestDTO struct {
		Name         string `json:"name" validate:"required"`
		Email        string `json:"email" validate:"required,email"`
		Password     string `json:"password" validate:"required,gte=8"`
		Confirmation string `json:"confirmation" validate:"required,gte=8"`
	}

	UserDTO struct {
		Name    string  `json:"name"`
		Email   string  `json:"email"`
		ID      uint    `json:"id"`
		Balance float64 `json:"balance"`
		IsAdmin bool    `json:"isAdmin"`
	}

	LoginRequest struct {
		Email    string `json:"email" validate:"required,email"`
		Password string `json:"password" validate:"required,gte=8"`
	}

	LoginResponse struct {
		User  UserDTO `json:"user"`
		Token string  `json:"token"`
	}
)

func (c CreateUserRequestDTO) ToDAO() dao.User {
	return dao.User{
		Name:     c.Name,
		Email:    c.Email,
		Password: c.Password,
	}
}

func NewUserDTO(user dao.User) UserDTO {
	return UserDTO{
		Name:    user.Name,
		Email:   user.Email,
		ID:      user.ID,
		Balance: user.Balance,
		IsAdmin: user.IsAdmin,
	}
}
