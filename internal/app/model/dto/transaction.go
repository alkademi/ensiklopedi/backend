package dto

import (
	"github.com/xendit/xendit-go/ewallet"
	transaction_status "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao/trxStatus"
)

type (
	TransactionStatusDTO struct {
		Amount   int                                  `json:"amount"`
		Email    string                               `json:"email"`
		Book     BookDTO                              `json:"book"`
		Status   transaction_status.TransactionStatus `json:"status"`
		Metadata map[string]interface{}               `json:"metadata"`
	}

	PurchaseBookDTO struct {
		BookId        uint                    `json:"book_id"`
		PaymentMethod string                  `json:"payment_method"`
		Metadata      *map[string]interface{} `json:"-"`
		PhoneNumber   *string                 `json:"mobile_number"`
	}

	XenditCallbackDTO struct {
		Event      string                        `json:"event"`
		Created    string                        `json:"created"`
		BusinessID string                        `json:"business_id"`
		Data       ewallet.EWalletChargeResponse `json:"data"`
	}
)
