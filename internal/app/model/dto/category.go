package dto

import "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"

type (
	CreateCategoryRequestDTO struct {
		CategoryName string `json:"category" validate:"required"`
	}

	CategoryDTO struct {
		CategoryName string `json:"name"`
		ID           uint   `json:"id"`
	}

	DeleteCategoryRequestDTO struct {
		ID uint `json:"id"`
	}
)

func NewCategoryDTO(c dao.Category) CategoryDTO {
	return CategoryDTO{
		CategoryName: c.CategoryName,
		ID:           c.ID,
	}
}

func (c CreateCategoryRequestDTO) ToDAO() dao.Category {
	return dao.Category{
		CategoryName: c.CategoryName,
	}
}
