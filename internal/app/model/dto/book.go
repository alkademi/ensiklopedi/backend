package dto

import (
	"mime/multipart"

	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
)

type (
	CreateBookRequestDTO struct {
		Title       string
		Price       int
		Author      string
		Description string
		Publisher   string
		ISBN        string
		Cover       multipart.FileHeader
		Content     multipart.FileHeader
		Category    []uint
	}

	UpdateBookRequestDTO struct {
		Title       *string `json:"title"`
		Price       *int    `json:"price"`
		Author      *string `json:"author"`
		Description *string `json:"description"`
		ISBN        *string `json:"isbn"`
		Publisher   *string `json:"publisher"`
	}

	BookDTO struct {
		Title       string   `json:"title"`
		Price       int      `json:"price"`
		Author      string   `json:"author"`
		Description string   `json:"description"`
		Publisher   string   `json:"publisher"`
		ISBN        string   `json:"isbn"`
		Cover       string   `json:"cover"`
		Content     string   `json:"content"`
		ID          uint     `json:"id"`
		IsOwned     *bool    `json:"isOwned,omitempty"`
		Progress    *int     `json:"progress,omitempty"`
		Category    []string `json:"category"`
	}

	CollectionBookDTO struct {
		ID      uint   `json:"id"`
		Title   string `json:"title"`
		Author  string `json:"author"`
		Cover   string `json:"cover"`
		Content string `json:"content"`
	}

	SearchFilterBookDTO struct {
		ID       uint     `json:"id"`
		Title    string   `json:"title"`
		Author   string   `json:"author"`
		Price    int      `json:"price"`
		Cover    string   `json:"cover"`
		IsOwned  bool     `json:"isOwned"`
		Category []string `json:"category"`
	}

	BookMapping map[string][]BookDTO

	HomeDTO struct {
		MyItems    []CollectionDTO `json:"my_items"`
		Categories []CategoryDTO   `json:"categories"`
		Books      BookMapping     `json:"books"`
	}
)

func NewBookDTO(book dao.Book) BookDTO {
	return BookDTO{
		Title:       book.Title,
		Price:       book.Price,
		Author:      book.Author,
		Description: book.Description,
		Publisher:   book.Publisher,
		ISBN:        book.ISBN,
		Cover:       book.Cover,
		Content:     book.Content,
		Category:    []string{},
	}
}

func (b CreateBookRequestDTO) ToDAO(coverCode, contentCode string) dao.Book {
	return dao.Book{
		Title:       b.Title,
		Price:       b.Price,
		Author:      b.Author,
		Description: b.Description,
		Publisher:   b.Publisher,
		ISBN:        b.ISBN,
		Cover:       coverCode,
		Content:     contentCode,
	}
}

// func NewBookDTO(book dao.Book, categories []string) BookDTO {
// 	return BookDTO{
// 		ID:          book.ID,
// 		Title:       book.Title,
// 		Author:      book.Author,
// 		Description: book.Description,
// 		Price:       book.Price,
// 		Cover:       book.Cover,
// 		Content:     book.Content,
// 		Category:    categories,
// 	}
// }

func NewCollectionBookDTO(book dao.Book) CollectionBookDTO {
	return CollectionBookDTO{
		ID:      book.ID,
		Title:   book.Title,
		Author:  book.Author,
		Cover:   book.Cover,
		Content: book.Content,
	}
}

func NewSearchFilterBookDTO(book dao.Book, categories []string, collections MyCollectionDTO) SearchFilterBookDTO {
	isOwned := false
	for _, collection := range collections.Collections {
		if collection.BookID == book.ID {
			isOwned = true
			break
		}
	}

	return SearchFilterBookDTO{
		ID:       book.ID,
		Title:    book.Title,
		Author:   book.Author,
		Price:    book.Price,
		Cover:    book.Cover,
		IsOwned:  isOwned,
		Category: categories,
	}
}
