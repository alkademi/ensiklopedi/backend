// Code generated by mockery v2.10.4. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// HTTPConfig is an autogenerated mock type for the HTTPConfig type
type HTTPConfig struct {
	mock.Mock
}

// ListenAddr provides a mock function with given fields:
func (_m *HTTPConfig) ListenAddr() string {
	ret := _m.Called()

	var r0 string
	if rf, ok := ret.Get(0).(func() string); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}
