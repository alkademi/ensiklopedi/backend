package config

import (
	"fmt"

	"github.com/xendit/xendit-go"
)

type paymentConfig struct {
	PaymentConfig *paymentConfigImpl `mapstructure:"PayementGateway"`
}

type paymentConfigImpl struct {
	APIKey string `mapstructure:"APIKey"`
}

type PaymentConfig interface {
	APIKey() string
}

func NewPayment(viperLoader ViperLoader) PaymentConfig {
	cfg := &paymentConfig{}

	viperLoader.Unmarshal(cfg)
	fmt.Println("sampe sini")
	xendit.Opt.SecretKey = cfg.APIKey()

	return cfg
}

func (a *paymentConfig) APIKey() string {
	return a.PaymentConfig.APIKey
}
