package controllers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"go.uber.org/fx"
)

type categoryControllerParams struct {
	fx.In

	Service services.CategoryService
}

type CategoryController interface {
	GetAll(ctx echo.Context) error
	CreateCategory(ctx echo.Context) error
	GetCategory(ctx echo.Context) error
	UpdateCategory(ctx echo.Context) error
	DeleteCategory(ctx echo.Context) error
}

func NewCategoryController(params categoryControllerParams) CategoryController {
	return &params
}

func (c categoryControllerParams) GetAll(ctx echo.Context) error {
	result, err := c.Service.GetAllCategory()

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

// CreateOrder godoc
// @Summary Get a new book by id
// @Description Get a book detail with the input id
// @Tags book
// @Accept  json
// @Produce  json
// @Param book_info body dto.BookDTO true "get book"
// @Success 200 {object} dto.BookDTO
// @Router /api/v1/book/{id_book} [get]
func (c categoryControllerParams) CreateCategory(ctx echo.Context) error {
	newCategory := dto.CreateCategoryRequestDTO{}
	ctx.Bind(&newCategory)

	result, err := c.Service.CreateCategory(newCategory)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

func (c categoryControllerParams) GetCategory(ctx echo.Context) error {
	idInString := ctx.Param("id")
	id, _ := strconv.ParseUint(idInString, 10, 32)
	result, err := c.Service.GetCategory(id)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

func (c categoryControllerParams) UpdateCategory(ctx echo.Context) error {
	newCategory := dto.CategoryDTO{}
	ctx.Bind(&newCategory)

	result, err := c.Service.UpdateCategory(newCategory)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

func (c categoryControllerParams) DeleteCategory(ctx echo.Context) error {
	categoryId := dto.DeleteCategoryRequestDTO{}
	ctx.Bind(&categoryId)

	err := c.Service.DeleteCategory(categoryId)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status:  http.StatusOK,
			Message: "delete category with id = " + strconv.Itoa(int(categoryId.ID)),
		}
	}

	return resp.JSON(ctx)
}
