package controllers

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/xendit/xendit-go/ewallet"
	m_config "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/config/mocks"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	m_services "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services/mocks"
	"gorm.io/gorm"
)

func TestPurchaseBook(t *testing.T) {
	purchase := `{
		"book_id":         5,
		"payment_method":        "ID_OVO",
		"metadata": {
			"payment_method": "ID_OVO",
			"mobile_number": ""
		},
		"mobile_number": ""
	}`

	testCases := []struct {
		name               string
		transactionService func() *m_services.TransactionService
		echoCtx            func() echo.Context
		expectedError      error
		jwt                func() *lib.JWT
	}{
		{
			name: "success purchase book ovo",
			transactionService: func() *m_services.TransactionService {
				mck := &m_services.TransactionService{}
				metadata := map[string]interface{}{}
				metadata["payment_method"] = "ID_OVO"
				metadata["mobile_number"] = ""

				mck.On("BuyBook", uint(13), uint(5), metadata).
					Return(dto.TransactionStatusDTO{}, nil)

				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(purchase)
				ctx := echoContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.transactionService()

			var transactionParams transactionControllerParams
			transactionParams.TransactionService = mck
			transactionParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(100000),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := transactionParams.JWT.Encode(claims)

			ctrl := NewTransactionController(transactionParams)

			ctx := tc.echoCtx()
			ctx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.PurchaseBook(ctx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestXenditCallback(t *testing.T) {
	req_string := `{
		"event": "",
		"created": "",
		"business_id": "",
		"data": {}
	}`

	testCases := []struct {
		name               string
		transactionService func() *m_services.TransactionService
		echoCtx            func() echo.Context
		expectedError      error
		jwt                func() *lib.JWT
	}{
		{
			name: "success xendit callback",
			transactionService: func() *m_services.TransactionService {
				mck := &m_services.TransactionService{}
				request := dto.XenditCallbackDTO{
					Event:      "",
					Created:    "",
					BusinessID: "",
					Data:       ewallet.EWalletChargeResponse{},
				}

				mck.On("XenditCallback", request).
					Return(dto.TransactionStatusDTO{}, nil)

				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(req_string)
				ctx := echoContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.transactionService()

			var transactionParams transactionControllerParams
			transactionParams.TransactionService = mck
			transactionParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(100000),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := transactionParams.JWT.Encode(claims)

			ctrl := NewTransactionController(transactionParams)

			ctx := tc.echoCtx()
			ctx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.XenditCallback(ctx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
