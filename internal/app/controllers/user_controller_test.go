package controllers

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	errors "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	m_services "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services/mocks"
)

func TestGetUser(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		userService   func() *m_services.UserService
		echoCtx       echo.Context
		expectedError error
	}{
		{
			name: "success get user",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("GetUser", uint64(3)).
					Return(dao.User{}, nil).
					Once()
				return mck
			},
			echoCtx:       echoUserContext(http.MethodGet, nil, e),
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.userService()

			var userParams userControllerParams
			userParams.Service = mck

			ctrl := NewUserController(userParams)
			tc.echoCtx.SetPath("/:id")
			tc.echoCtx.SetParamNames("id")
			tc.echoCtx.SetParamValues("3")

			err := ctrl.GetUser(tc.echoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestCreateUser(t *testing.T) {
	user1 := `{
		"name":         "User1",
		"email":        "user1@gmail.com",
		"password":     "password",
		"confirmation": "password"
	}`
	user2 := `{
		"name":         "User2",
		"email":        "user2@gmail.com",
		"password":     "password",
		"confirmation": "passworddd"
	}`
	user3 := `{
		"name":         "User1",
		"email":        "user1@gmail.com",
		"password":     "password",
		"confirmation": "password"
	}`

	testCases := []struct {
		name          string
		userService   func() *m_services.UserService
		echoCtx       func() echo.Context
		expectedError error
	}{
		{
			name: "success create user",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("CreateUser", dto.CreateUserRequestDTO{
					Name:         "User1",
					Email:        "user1@gmail.com",
					Password:     "password",
					Confirmation: "password",
				}).
					Return(dto.UserDTO{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(user1)
				ctx := echoUserContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail create user (password doesnt match)",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("CreateUser", dto.CreateUserRequestDTO{
					Name:         "User2",
					Email:        "user2@gmail.com",
					Password:     "password",
					Confirmation: "passworddd",
				}).
					Return(dto.UserDTO{}, errors.ErrPasswordDoesNotMatch).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(user2)
				ctx := echoUserContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail create user (email already exists)",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("CreateUser", dto.CreateUserRequestDTO{
					Name:         "User1",
					Email:        "user1@gmail.com",
					Password:     "password",
					Confirmation: "password",
				}).
					Return(dto.UserDTO{}, errors.ErrEmailAlreadyExist).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(user3)
				ctx := echoUserContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.userService()

			var userParams userControllerParams
			userParams.Service = mck

			ctrl := NewUserController(userParams)
			err := ctrl.CreateUser(tc.echoCtx())

			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}

		})
	}
}

func TestLogin(t *testing.T) {
	e := echo.New()
	user1 := `{
		"email":        "email@gmail.com",
		"password":     "password"
	}`
	user2 := `{
		"email":        "user1@gmail.com",
		"password":     "password"
	}`

	testCases := []struct {
		name          string
		userService   func() *m_services.UserService
		echoCtx       func() echo.Context
		expectedError error
	}{
		{
			name: "success login",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("Login", dto.LoginRequest{
					Email:    "email@gmail.com",
					Password: "password",
				}).
					Return(dto.LoginResponse{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				body := strings.NewReader(user1)
				ctx := echoUserContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail login",
			userService: func() *m_services.UserService {
				mck := &m_services.UserService{}
				mck.On("Login", dto.LoginRequest{
					Email:    "user1@gmail.com",
					Password: "password",
				}).
					Return(dto.LoginResponse{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				body := strings.NewReader(user2)
				ctx := echoUserContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.userService()

			var userParams userControllerParams
			userParams.Service = mck

			ctrl := NewUserController(userParams)
			err := ctrl.Login(tc.echoCtx())

			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func echoUserContext(method string, body io.Reader, e *echo.Echo) echo.Context {
	req := httptest.NewRequest(method, "/", body)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()

	return e.NewContext(req, rec)
}
