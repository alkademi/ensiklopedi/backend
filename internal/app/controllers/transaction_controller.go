package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/utils"
	"go.uber.org/fx"
)

type transactionControllerParams struct {
	fx.In

	services.TransactionService
	lib.JWT
}

type TransactionController interface {
	PurchaseBook(ctx echo.Context) error
	XenditCallback(ctx echo.Context) error
}

func NewTransactionController(params transactionControllerParams) TransactionController {
	return &params
}

func (t transactionControllerParams) PurchaseBook(ctx echo.Context) error {
	request := dto.PurchaseBookDTO{}
	if err := ctx.Bind(&request); err != nil {
		return err
	}
	token, err := utils.ExtractToken(ctx)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromToken(token, t.JWT)
	if err != nil {
		return err
	}
	request.Metadata = &map[string]interface{}{}
	(*request.Metadata)["payment_method"] = request.PaymentMethod
	if request.PhoneNumber != nil {
		(*request.Metadata)["mobile_number"] = *request.PhoneNumber
	}
	result, err := t.TransactionService.BuyBook(user.ID, request.BookId, *request.Metadata)
	if err != nil {
		return err
	}
	return lib.Response{
		Status:  http.StatusOK,
		Data:    result,
		Message: "purchase success",
	}.JSON(ctx)
}

func (t transactionControllerParams) XenditCallback(ctx echo.Context) error {
	request := dto.XenditCallbackDTO{}
	if err := ctx.Bind(&request); err != nil {
		return err
	}
	result, err := t.TransactionService.XenditCallback(request)
	if err != nil {
		return err
	}
	return lib.Response{
		Status:  http.StatusOK,
		Data:    result,
		Message: "success",
	}.JSON(ctx)
}
