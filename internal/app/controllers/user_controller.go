package controllers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"go.uber.org/fx"
)

type userControllerParams struct {
	fx.In

	Service services.UserService
	lib.Hash
}

type UserController interface {
	GetUser(ctx echo.Context) error
	CreateUser(ctx echo.Context) error
	Login(ctx echo.Context) error
}

func NewUserController(params userControllerParams) UserController {
	return &params
}

// CreateOrder godoc
// @Summary Create a new user
// @Description Create a new user with the input paylod
// @Tags user
// @Accept  json
// @Produce  json
// @Param user_info body dto.CreateUserRequestDTO true "create user"
// @Success 200 {object} dto.UserDTO
// @Router /api/v1/user/ [post]
func (c userControllerParams) GetUser(ctx echo.Context) error {
	idInString := ctx.Param("id")
	id, _ := strconv.ParseUint(idInString, 10, 32)
	user, err := c.Service.GetUser(id)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data: dto.UserDTO{
				Name:    user.Name,
				Email:   user.Email,
				ID:      user.ID,
				Balance: user.Balance,
			},
		}
	}
	return resp.JSON(ctx)
}

func (c userControllerParams) CreateUser(ctx echo.Context) error {
	user := dto.CreateUserRequestDTO{}

	if err := ctx.Bind(&user); err != nil {
		return err
	}

	result, err := c.Service.CreateUser(user)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusBadRequest,
			Data:    result,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}
	return resp.JSON(ctx)
}

// CreateOrder godoc
// @Summary Login
// @Tags user
// @Accept  json
// @Produce  json
// @Param login_info body dto.LoginRequest true "user login info"
// @Success 200 {object} dto.LoginResponse
// @Router /api/v1/user/login [post]
func (c userControllerParams) Login(ctx echo.Context) error {
	loginRequest := dto.LoginRequest{}
	ctx.Bind(&loginRequest)
	res, err := c.Service.Login(loginRequest)
	if err != nil {
		return err
	}

	return lib.Response{
		Status: http.StatusOK,
		Data:   res,
	}.JSON(ctx)
}
