package controllers

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	m_config "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/config/mocks"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	m_services "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services/mocks"
	"gorm.io/gorm"
)

func TestGetCollectionData(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name              string
		collectionService func() *m_services.CollectionService
		echoCtx           echo.Context
		expectedError     error
		jwt               func() *lib.JWT
	}{
		{
			name: "success get collection",
			collectionService: func() *m_services.CollectionService {
				mck := &m_services.CollectionService{}
				mck.On("GetCollectionByUserId", uint(5)).
					Return(dto.MyCollectionDTO{}, nil).
					Once()
				return mck
			},
			echoCtx:       echoContext(http.MethodGet, nil, e),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.collectionService()

			var collectionParams collectionControllerParams
			collectionParams.Service = mck
			collectionParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 5,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := collectionParams.JWT.Encode(claims)

			ctrl := NewCollectionController(collectionParams)
			tc.echoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.GetCollectionData(tc.echoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestTrackProgress(t *testing.T) {
	testCases := []struct {
		name              string
		collectionService func() *m_services.CollectionService
		echoCtx           echo.Context
		expectedError     error
		jwt               func() *lib.JWT
	}{
		{
			name: "success track progress",
			collectionService: func() *m_services.CollectionService {
				mck := &m_services.CollectionService{}
				mck.On("TrackProgress", uint(5), uint(5), 70).
					Return(dto.ProgressDTO{}, nil).
					Once()
				return mck
			},
			echoCtx:       echoContext(http.MethodPatch, nil, echo.New()),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.collectionService()

			var collectionParams collectionControllerParams
			collectionParams.Service = mck
			collectionParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 5,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := collectionParams.JWT.Encode(claims)

			ctrl := NewCollectionController(collectionParams)
			tc.echoCtx.SetPath("/:bookId/:progress")
			tc.echoCtx.SetParamNames("bookId", "progress")
			tc.echoCtx.SetParamValues("5", "70")
			tc.echoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.TrackProgress(tc.echoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
