package controllers

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/utils"
	"go.uber.org/fx"
)

type bookControllerParams struct {
	fx.In

	Service services.BookService
	lib.JWT
}

type BookController interface {
	GetAllBook(ctx echo.Context) error
	GetBook(ctx echo.Context) error
	GetHomeData(ctx echo.Context) error
	CreateBook(ctx echo.Context) error
	SearchBook(ctx echo.Context) error
	FilterBook(ctx echo.Context) error
	UpdateBook(ctx echo.Context) error
	DeleteBook(ctx echo.Context) error
}

func NewBookController(params bookControllerParams) BookController {
	return &params
}

func (c bookControllerParams) GetAllBook(ctx echo.Context) error {
	books, err := c.Service.GetAllBook()
	if err != nil {
		return err
	}
	return lib.Response{
		Status:  http.StatusOK,
		Data:    books,
		Message: "get all books success",
	}.JSON(ctx)
}

func (c bookControllerParams) DeleteBook(ctx echo.Context) error {
	idInString := ctx.Param("id")
	id, _ := strconv.ParseUint(idInString, 10, 32)
	err := c.Service.DeleteBook(id)
	if err != nil {
		return err
	}
	return lib.Response{
		Status:  http.StatusOK,
		Message: "delete book success",
	}.JSON(ctx)
}

// CreateOrder godoc
// @Summary Get a new book by id
// @Description Get a book detail with the input id
// @Tags book
// @Accept  json
// @Produce  json
// @Param book_info body dto.BookDTO true "get book"
// @Success 200 {object} dto.BookDTO
// @Router /api/v1/book/{id_book} [get]
func (c bookControllerParams) GetBook(ctx echo.Context) error {
	idInString := ctx.Param("id")
	id_book, _ := strconv.ParseUint(idInString, 10, 32)

	token, err := utils.ExtractToken(ctx)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromToken(token, c.JWT)

	book, err := c.Service.GetBook(uint64(user.ID), id_book)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		categories := []string{}
		categories_resp, err2 := c.Service.GetCategoryByBookId(id_book)

		if err2 == nil {
			for _, category := range categories_resp {
				categories = append(categories, category.CategoryName)
			}

			resp = lib.Response{
				Status: http.StatusOK,
				Data: dto.BookDTO{
					Title:       book.Title,
					Price:       book.Price,
					Author:      book.Author,
					Description: book.Description,
					Cover:       book.Cover,
					Content:     book.Content,
					ID:          uint(id_book),
					IsOwned:     book.IsOwned,
					Progress:    book.Progress,
					Category:    categories,
					Publisher:   book.Publisher,
					ISBN:        book.ISBN,
				},
			}
		} else {
			resp = lib.Response{
				Status:  http.StatusNotFound,
				Message: err2.Error(),
			}
		}
	}

	return resp.JSON(ctx)
}

func (c bookControllerParams) GetHomeData(ctx echo.Context) error {
	// get user from token
	token, err := utils.ExtractToken(ctx)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromToken(token, c.JWT)

	// get all data for home
	result, err := c.Service.GetHomeData(user.ID)

	var resp lib.Response
	// checking if there is an error
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusNotFound,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

func (c bookControllerParams) SearchBook(ctx echo.Context) error {
	query := ctx.Param("search")
	params := [2]string{query, ""}
	user := dto.UserDTO{}
	token, err := utils.ExtractToken(ctx)
	if err == nil {
		user, _ = utils.GetUserFromToken(token, c.JWT)
		params[1] = strconv.FormatUint(uint64(user.ID), 10)
	}

	result, err := c.Service.SearchBook(params)
	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}

func (c bookControllerParams) FilterBook(ctx echo.Context) error {
	param := ctx.Param("category")
	queries := strings.Split(param, ",")
	params := []string{}
	token, err := utils.ExtractToken(ctx)
	user := dto.UserDTO{}
	if err == nil {
		user, _ = utils.GetUserFromToken(token, c.JWT)
		params = append(params, strconv.FormatUint(uint64(user.ID), 10))
	} else {
		params = append(params, "")
	}

	params = append(params, queries...)

	result, err := c.Service.FilterBook(params)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
	} else {
		resp = lib.Response{
			Status: http.StatusOK,
			Data:   result,
		}
	}

	return resp.JSON(ctx)
}
func (c bookControllerParams) UpdateBook(ctx echo.Context) error {
	idInString := ctx.Param("id")
	id, _ := strconv.ParseUint(idInString, 10, 32)
	updateRequest := dto.UpdateBookRequestDTO{}
	ctx.Bind(&updateRequest)
	var book dao.Book
	var err error
	if book, err = c.Service.UpdateBook(uint(id), updateRequest); err != nil {
		return err
	}

	bookDto := dto.NewBookDTO(book)

	categories := []string{}
	categories_resp, err := c.Service.GetCategoryByBookId(id)

	if err == nil {
		for _, category := range categories_resp {
			categories = append(categories, category.CategoryName)
		}

		bookDto.Category = categories
	}

	return lib.Response{
		Status:  http.StatusOK,
		Data:    bookDto,
		Message: "update book success",
	}.JSON(ctx)

}

func (c bookControllerParams) CreateBook(ctx echo.Context) error {
	title := ctx.FormValue("title")
	price, _ := strconv.Atoi(ctx.FormValue("price"))
	author := ctx.FormValue("author")
	description := ctx.FormValue("description")
	cover, _ := ctx.FormFile("cover")
	content, _ := ctx.FormFile("content")
	category := ctx.FormValue("category")
	isbn := ctx.FormValue("isbn")
	publisher := ctx.FormValue("publisher")

	splittedCategory := strings.Split(category, ",")

	categories := make([]uint, len(splittedCategory))

	for i, c := range splittedCategory {
		u, _ := strconv.Atoi(c)
		categories[i] = uint(u)
	}

	requestDTO := dto.CreateBookRequestDTO{
		Title:       title,
		Price:       price,
		Author:      author,
		Description: description,
		Cover:       *cover,
		Content:     *content,
		Category:    categories,
		Publisher:   publisher,
		ISBN:        isbn,
	}

	book, err := c.Service.CreateBook(requestDTO)

	if err != nil {
		return err
	}

	return lib.Response{
		Status:  http.StatusOK,
		Data:    book,
		Message: "add book success",
	}.JSON(ctx)

}
