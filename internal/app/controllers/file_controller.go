package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"go.uber.org/fx"
)

type fileControllerParams struct {
	fx.In

	FileService services.FileService
}

type FileController interface {
	GetFile(ctx echo.Context) error
	UploadFile(ctx echo.Context) error
}

func NewFileController(params fileControllerParams) FileController {
	return &params
}

func (p fileControllerParams) GetFile(ctx echo.Context) error {
	fileCode := ctx.Param("id")

	fileBytes, _, err := p.FileService.GetFile(fileCode)
	if err != nil {
		return err
	}

	mimeType := http.DetectContentType(fileBytes)

	return ctx.Blob(http.StatusOK, mimeType, fileBytes)
}

func (p fileControllerParams) UploadFile(ctx echo.Context) error {
	file, err := ctx.FormFile("file")
	if err != nil {
		return err
	}
	uploadedFile, err := p.FileService.UploadFile(*file)
	if err != nil {
		return err
	}

	return lib.Response{
		Status:  http.StatusOK,
		Data:    uploadedFile,
		Message: "upload file successfull",
	}.JSON(ctx)

}
