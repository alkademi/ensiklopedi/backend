package controllers

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	m_services "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services/mocks"
)

func TestCreateCategory(t *testing.T) {
	cat := `{
		"category": "Self Development"
	}`

	testCases := []struct {
		name            string
		categoryService func() *m_services.CategoryService
		echoCtx         func() echo.Context
		expectedError   error
	}{
		{
			name: "success create category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("CreateCategory", dto.CreateCategoryRequestDTO{
					CategoryName: "Self Development",
				}).
					Return(dao.Category{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat)
				ctx := echoCategoryContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail create category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("CreateCategory", dto.CreateCategoryRequestDTO{
					CategoryName: "Self Development",
				}).
					Return(dao.Category{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat)
				ctx := echoCategoryContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.categoryService()

			var categoryParams categoryControllerParams
			categoryParams.Service = mck

			ctrl := NewCategoryController(categoryParams)
			err := ctrl.CreateCategory(tc.echoCtx())

			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}

		})
	}
}

func TestGetCategory(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name            string
		categoryService func() *m_services.CategoryService
		echoCtx         echo.Context
		expectedError   error
	}{
		{
			name: "success get category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("GetCategory", uint64(3)).
					Return(dto.CategoryDTO{}, nil).
					Once()
				return mck
			},
			echoCtx:       echoCategoryContext(http.MethodGet, nil, e),
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.categoryService()

			var categoryParams categoryControllerParams
			categoryParams.Service = mck

			ctrl := NewCategoryController(categoryParams)
			tc.echoCtx.SetPath("/:id")
			tc.echoCtx.SetParamNames("id")
			tc.echoCtx.SetParamValues("3")

			err := ctrl.GetCategory(tc.echoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestUpdateCategory(t *testing.T) {
	cat1 := `{
		"name": "Update ID 5",
		"id":           5
	}`

	cat2 := `{
		"name": "Update ID 100",
		"id":          100
	}`

	testCases := []struct {
		name            string
		categoryService func() *m_services.CategoryService
		echoCtx         func() echo.Context
		expectedError   error
	}{
		{
			name: "success update category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("UpdateCategory", dto.CategoryDTO{
					CategoryName: "Update ID 5",
					ID:           uint(5),
				}).
					Return(dto.CategoryDTO{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat1)
				ctx := echoCategoryContext(http.MethodPut, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail update category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("UpdateCategory", dto.CategoryDTO{
					CategoryName: "Update ID 100",
					ID:           uint(100),
				}).
					Return(dto.CategoryDTO{}, nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat2)
				ctx := echoCategoryContext(http.MethodPost, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.categoryService()

			var categoryParams categoryControllerParams
			categoryParams.Service = mck

			ctrl := NewCategoryController(categoryParams)

			err := ctrl.UpdateCategory(tc.echoCtx())
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestDeleteCategory(t *testing.T) {
	cat1 := `{
		"id":           5
	}`

	cat2 := `{
		"id":          100
	}`

	testCases := []struct {
		name            string
		categoryService func() *m_services.CategoryService
		echoCtx         func() echo.Context
		expectedError   error
	}{
		{
			name: "success delete category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("DeleteCategory", dto.DeleteCategoryRequestDTO{
					ID: uint(5),
				}).
					Return(nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat1)
				ctx := echoCategoryContext(http.MethodDelete, body, e)

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail delete category",
			categoryService: func() *m_services.CategoryService {
				mck := &m_services.CategoryService{}
				mck.On("DeleteCategory", dto.DeleteCategoryRequestDTO{
					ID: uint(100),
				}).
					Return(nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(cat2)
				ctx := echoCategoryContext(http.MethodDelete, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.categoryService()

			var categoryParams categoryControllerParams
			categoryParams.Service = mck

			ctrl := NewCategoryController(categoryParams)

			err := ctrl.DeleteCategory(tc.echoCtx())
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func echoCategoryContext(method string, body io.Reader, e *echo.Echo) echo.Context {
	req := httptest.NewRequest(method, "/", body)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	// req.Header.Set("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJiYWxhbmNlIjowLCJlbWFpbCI6ImVtYWlsQGVtYWlsLmNvbSIsImV4cCI6MTY0OTk3NzcwNSwiaWQiOjYsImlzQWRtaW4iOmZhbHNlLCJuYW1lIjoibmFtYSBvcmFuZyJ9.sjofU4YkOHCe1dPpLkjt0_wZ3xgkEPIXZVMsvrclVUA")

	rec := httptest.NewRecorder()

	return e.NewContext(req, rec)
}
