package controllers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/utils"
	"go.uber.org/fx"
)

type collectionControllerParams struct {
	fx.In

	Service services.CollectionService
	lib.JWT
}

type CollectionController interface {
	GetCollectionData(ctx echo.Context) error
	TrackProgress(ctx echo.Context) error
}

func NewCollectionController(params collectionControllerParams) CollectionController {
	return &params
}

func (c collectionControllerParams) GetCollectionData(ctx echo.Context) error {
	token, err := utils.ExtractToken(ctx)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromToken(token, c.JWT)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
	} else {
		result, err1 := c.Service.GetCollectionByUserId(user.ID)
		if err1 != nil {
			resp = lib.Response{
				Status:  http.StatusNotFound,
				Message: err1.Error(),
			}
		} else {
			resp = lib.Response{
				Status: http.StatusOK,
				Data:   result,
			}

		}
	}

	return resp.JSON(ctx)
}

func (c collectionControllerParams) TrackProgress(ctx echo.Context) error {
	bookParam := ctx.Param("bookId")
	bookID, _ := strconv.ParseUint(bookParam, 10, 32)
	progParam := ctx.Param("progress")
	progress, _ := strconv.ParseInt(progParam, 10, 32)

	token, err := utils.ExtractToken(ctx)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromToken(token, c.JWT)

	var resp lib.Response
	if err != nil {
		resp = lib.Response{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		}
	} else {
		result, err1 := c.Service.TrackProgress(user.ID, uint(bookID), int(progress))
		if err1 != nil {
			resp = lib.Response{
				Status:  http.StatusNotFound,
				Message: err1.Error(),
			}
		} else {
			resp = lib.Response{
				Status: http.StatusOK,
				Data:   result,
			}

		}
	}

	return resp.JSON(ctx)
}
