package controllers

import (
	"encoding/json"
	"io"
	"strings"

	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	m_config "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/config/mocks"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	m_services "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/services/mocks"
	"gorm.io/gorm"
)

func TestGetAllBook(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		echoCtx       echo.Context
		expectedError error
	}{
		{
			name: "success get all books",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("GetAllBook").
					Return([]dto.BookDTO{}, nil).
					Once()
				return mck
			},
			echoCtx:       echoContext(http.MethodGet, nil, e),
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck

			ctrl := NewBookController(bookParams)

			err := ctrl.GetAllBook(tc.echoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestGetBook(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		bookEchoCtx   echo.Context
		expectedError error
		jwt           func() *lib.JWT
	}{
		{
			name: "success get book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("GetBook", uint64(13), uint64(3)).
					Return(dto.BookDTO{}, nil).
					Once()
				mck.On("GetCategoryByBookId", uint64(3)).
					Return([]dao.Category{}, nil).
					Once()
				return mck
			},
			bookEchoCtx:   echoContext(http.MethodGet, nil, e),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck
			bookParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := bookParams.JWT.Encode(claims)

			ctrl := NewBookController(bookParams)
			tc.bookEchoCtx.SetPath("/:id")
			tc.bookEchoCtx.SetParamNames("id")
			tc.bookEchoCtx.SetParamValues("3")
			tc.bookEchoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.GetBook(tc.bookEchoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestGetHomeData(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		bookEchoCtx   echo.Context
		expectedError error
		jwt           func() *lib.JWT
	}{
		{
			name: "success get home data",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("GetHomeData", uint(13)).
					Return(dto.HomeDTO{}, nil).
					Once()
				return mck
			},
			bookEchoCtx:   echoContext(http.MethodGet, nil, e),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck
			bookParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := bookParams.JWT.Encode(claims)

			ctrl := NewBookController(bookParams)
			tc.bookEchoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.GetHomeData(tc.bookEchoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestSearchBook(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		bookEchoCtx   echo.Context
		expectedError error
		jwt           func() *lib.JWT
	}{
		{
			name: "success search book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("SearchBook", [2]string{"manager", "13"}).
					Return([]dto.SearchFilterBookDTO{}, nil).
					Once()
				return mck
			},
			bookEchoCtx:   echoContext(http.MethodGet, nil, e),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck
			bookParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := bookParams.JWT.Encode(claims)

			ctrl := NewBookController(bookParams)
			tc.bookEchoCtx.SetPath("/:search")
			tc.bookEchoCtx.SetParamNames("search")
			tc.bookEchoCtx.SetParamValues("manager")
			tc.bookEchoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.SearchBook(tc.bookEchoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestFilterBook(t *testing.T) {
	e := echo.New()

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		bookEchoCtx   echo.Context
		expectedError error
		jwt           func() *lib.JWT
	}{
		{
			name: "success filter book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("FilterBook", []string{"13", "Technology"}).
					Return([]dto.SearchFilterBookDTO{}, nil).
					Once()
				return mck
			},
			bookEchoCtx:   echoContext(http.MethodGet, nil, e),
			expectedError: nil,
			jwt: func() *lib.JWT {
				cfgMock := &m_config.JWTConfig{}
				cfgMock.On("Exp").
					Return(time.Duration(5) * time.Hour)
				cfgMock.On("Secret").
					Return("secret")
				params := lib.JwtParams{
					JWTConfig: cfgMock,
				}
				jwt := lib.NewJWT(params)
				return &jwt
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck
			bookParams.JWT = *tc.jwt()

			data, _ := json.Marshal(dto.NewUserDTO(dao.User{
				Name:     "Name",
				Email:    "email@gmail.com",
				Password: "password",
				Balance:  float64(0),
				IsAdmin:  false,
				Model: gorm.Model{
					ID: 13,
				},
			}))
			var claims map[string]interface{}
			json.Unmarshal(data, &claims)
			token, _ := bookParams.JWT.Encode(claims)

			ctrl := NewBookController(bookParams)
			tc.bookEchoCtx.SetPath("/:category")
			tc.bookEchoCtx.SetParamNames("category")
			tc.bookEchoCtx.SetParamValues("Technology")
			tc.bookEchoCtx.Request().Header.Set("Authorization", "Bearer "+token)

			err := ctrl.FilterBook(tc.bookEchoCtx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestUpdateBook(t *testing.T) {
	title := "Update Book"
	price := 20000
	author := "Update Author"
	description := "Update Description"

	book := `{
		"title": "Update Book",
		"price": 20000,
		"author": "Update Author",
		"description": "Update Description"
	}`

	// book := `{
	// 	"title": "Update Book",
	// 	"price": 20000,
	// 	"author": "Update Author",
	// 	"description": "Update Description"
	// }`

	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		echoCtx       func() echo.Context
		expectedError error
	}{
		{
			name: "success update book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}

				mck.On("UpdateBook", uint(3), dto.UpdateBookRequestDTO{
					Title:       &title,
					Price:       &price,
					Author:      &author,
					Description: &description,
				}).
					Return(dao.Book{}, nil)

				mck.On("GetCategoryByBookId", uint64(3)).
					Return([]dao.Category{}, nil)

				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				body := strings.NewReader(book)
				ctx := echoContext(http.MethodPut, body, e)

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck

			ctrl := NewBookController(bookParams)
			ctx := tc.echoCtx()
			ctx.SetPath("/:id")
			ctx.SetParamNames("id")
			ctx.SetParamValues("3")

			err := ctrl.UpdateBook(ctx)
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestDeleteBook(t *testing.T) {
	testCases := []struct {
		name          string
		bookService   func() *m_services.BookService
		echoCtx       func() echo.Context
		expectedError error
	}{
		{
			name: "success delete book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("DeleteBook", uint64(5)).
					Return(nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				ctx := echoContext(http.MethodDelete, nil, e)
				ctx.SetPath("/:id")
				ctx.SetParamNames("id")
				ctx.SetParamValues("5")

				return ctx
			},
			expectedError: nil,
		},
		{
			name: "fail delete book",
			bookService: func() *m_services.BookService {
				mck := &m_services.BookService{}
				mck.On("DeleteBook", uint64(100)).
					Return(nil).
					Once()
				return mck
			},
			echoCtx: func() echo.Context {
				e := echo.New()
				ctx := echoContext(http.MethodDelete, nil, e)
				ctx.SetPath("/:id")
				ctx.SetParamNames("id")
				ctx.SetParamValues("100")

				return ctx
			},
			expectedError: nil,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			mck := tc.bookService()

			var bookParams bookControllerParams
			bookParams.Service = mck

			ctrl := NewBookController(bookParams)

			err := ctrl.DeleteBook(tc.echoCtx())
			if tc.expectedError != nil {
				assert.Equal(t, tc.expectedError, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func echoContext(method string, body io.Reader, e *echo.Echo) echo.Context {
	req := httptest.NewRequest(method, "/", body)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	rec := httptest.NewRecorder()

	return e.NewContext(req, rec)
}
