package services

import (
	e "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/repo"
	"go.uber.org/fx"
)

type collectionServiceParams struct {
	fx.In

	CollectionRepo repo.CollectionRepo
}

type CollectionService interface {
	GetAllCollection() ([]dao.Collection, error)
	GetCollectionByUserId(id uint) (dto.MyCollectionDTO, error)
	TrackProgress(userID uint, bookID uint, progress int) (dto.ProgressDTO, error)
	GetProgress(userID uint, bookID uint) (int, error)
}

func NewCollectionService(params collectionServiceParams) CollectionService {
	return &params
}

func (u *collectionServiceParams) GetAllCollection() ([]dao.Collection, error) {
	return u.CollectionRepo.GetAllCollection()
}

func (u *collectionServiceParams) GetCollectionByUserId(id uint) (dto.MyCollectionDTO, error) {
	myCollection := dto.MyCollectionDTO{}
	collections, err := u.CollectionRepo.GetCollectionByUserId(uint64(id))

	if err != nil {
		return dto.MyCollectionDTO{}, err
	} else {
		myCollection = dto.NewMyCollectionDTO(collections, id)
	}
	return myCollection, nil
}

func (u *collectionServiceParams) TrackProgress(userID uint, bookID uint, progress int) (dto.ProgressDTO, error) {
	ret, err := u.CollectionRepo.TrackProgress(userID, bookID, progress)

	if err != nil {
		return dto.ProgressDTO{}, err
	}

	if ret.ID == 0 {
		return dto.ProgressDTO{}, e.ErrCollectionNotFound
	}

	return dto.NewProgressDTO(ret), nil
}

func (u *collectionServiceParams) GetProgress(userID uint, bookID uint) (int, error) {
	collections, err := u.CollectionRepo.GetCollectionByUserId(uint64(userID))

	if err != nil {
		return 0, err
	} else {
		for _, item := range collections {
			if item.BookID == bookID {
				return item.Progress, nil
			}
		}
	}

	return 0, nil
}
