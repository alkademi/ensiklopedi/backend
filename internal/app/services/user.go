package services

import (
	"encoding/json"

	errors "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/repo"
	"go.uber.org/fx"
)

type userServiceParams struct {
	fx.In

	UserRepo repo.UserRepo
	JWT      lib.JWT
	Hash     lib.Hash
}

type UserService interface {
	GetUser(id uint64) (dao.User, error)
	GetUserByEmail(email string) (dao.User, error)
	CreateUser(user dto.CreateUserRequestDTO) (dto.UserDTO, error)
	Login(loginRequest dto.LoginRequest) (dto.LoginResponse, error)
}

func NewUserService(params userServiceParams) UserService {
	return &params
}

func (u *userServiceParams) GetUser(id uint64) (dao.User, error) {
	return u.UserRepo.GetUser(id)
}

func (u *userServiceParams) GetUserByEmail(email string) (dao.User, error) {
	return u.UserRepo.GetUserByEmail(email)
}

func (u *userServiceParams) CreateUser(user dto.CreateUserRequestDTO) (dto.UserDTO, error) {
	if user.Password != user.Confirmation {
		return dto.UserDTO{}, errors.ErrPasswordDoesNotMatch
	}

	if _, err := u.UserRepo.GetUserByEmail(user.Email); err == nil {
		return dto.UserDTO{}, errors.ErrEmailAlreadyExist
	}

	if hashed, hasherr := u.Hash.Hash(user.Password); hasherr != nil {
		return dto.UserDTO{}, hasherr
	} else {
		user.Password = hashed
	}
	newUser, err := u.UserRepo.CreateUser(user.ToDAO())
	return dto.NewUserDTO(newUser), err
}

func (u *userServiceParams) Login(loginRequest dto.LoginRequest) (dto.LoginResponse, error) {
	user, err := u.UserRepo.GetUserByEmail(loginRequest.Email)
	if err != nil {
		return dto.LoginResponse{}, err
	}

	passwordValid := u.Hash.Compare(loginRequest.Password, user.Password)
	if !passwordValid {
		return dto.LoginResponse{}, errors.ErrWrongPassword
	}

	data, _ := json.Marshal(dto.NewUserDTO(user))
	var claims map[string]interface{}
	json.Unmarshal(data, &claims)
	token, err := u.JWT.Encode(claims)

	if err != nil {
		return dto.LoginResponse{}, err
	}

	return dto.LoginResponse{
		User: dto.UserDTO{
			Name:    user.Name,
			Email:   user.Email,
			ID:      user.ID,
			Balance: user.Balance,
			IsAdmin: user.IsAdmin,
		},
		Token: token,
	}, nil
}
