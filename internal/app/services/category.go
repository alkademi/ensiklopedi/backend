package services

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/repo"
	"go.uber.org/fx"
)

type categoryServiceParams struct {
	fx.In

	CategoryRepo repo.CategoryRepo
}

type CategoryService interface {
	CreateCategory(category dto.CreateCategoryRequestDTO) (dao.Category, error)
	GetCategory(id uint64) (dto.CategoryDTO, error)
	UpdateCategory(newCategory dto.CategoryDTO) (dto.CategoryDTO, error)
	DeleteCategory(categoryId dto.DeleteCategoryRequestDTO) error
	GetAllCategory() ([]dto.CategoryDTO, error)
}

func NewCategoryService(params categoryServiceParams) CategoryService {
	return &params
}

func (u categoryServiceParams) CreateCategory(category dto.CreateCategoryRequestDTO) (dao.Category, error) {
	return u.CategoryRepo.CreateCategory(category.ToDAO())
}

func (u categoryServiceParams) GetCategory(id uint64) (dto.CategoryDTO, error) {
	category, err := u.CategoryRepo.GetCategory(id)
	res := dto.CategoryDTO{}

	if err != nil {
		return res, err
	} else {
		res = dto.CategoryDTO{
			CategoryName: category.CategoryName,
			ID:           category.ID,
		}
	}

	return res, nil
}

func (u categoryServiceParams) UpdateCategory(newCategory dto.CategoryDTO) (dto.CategoryDTO, error) {
	category, err := u.CategoryRepo.UpdateCategory(newCategory)
	res := dto.CategoryDTO{}

	if err != nil {
		return res, err
	} else {
		res = dto.CategoryDTO{
			CategoryName: category.CategoryName,
			ID:           newCategory.ID,
		}
	}

	return res, nil
}

func (u categoryServiceParams) DeleteCategory(categoryId dto.DeleteCategoryRequestDTO) error {
	return u.CategoryRepo.DeleteCategory(categoryId)
}

func (u categoryServiceParams) GetAllCategory() ([]dto.CategoryDTO, error) {
	categories, err := u.CategoryRepo.GetAllCategories()
	if err != nil {
		return []dto.CategoryDTO{}, err
	}

	result := make([]dto.CategoryDTO, len(categories))
	for i, c := range categories {
		result[i] = dto.NewCategoryDTO(c)
	}

	return result, nil
}
