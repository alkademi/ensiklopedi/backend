package services

import (
	"strconv"

	e "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/repo"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/utils"
	"go.uber.org/fx"
)

type bookServiceParams struct {
	fx.In
	FileService
	BookRepo          repo.BookRepo
	CategoryRepo      repo.CategoryRepo
	CollectionRepo    repo.CollectionRepo
	MemberRepo        repo.MemberRepo
	CollectionService CollectionService
}

type BookService interface {
	GetBook(id_user uint64, id_book uint64) (dto.BookDTO, error)
	GetHomeData(id uint) (dto.HomeDTO, error)
	DeleteBook(id uint64) error
	GetAllBook() ([]dto.BookDTO, error)
	GetBooksByCategoryId(id uint64) ([]dao.Book, error)
	CreateBook(book dto.CreateBookRequestDTO) (dao.Book, error)
	UpdateBook(id uint, book dto.UpdateBookRequestDTO) (dao.Book, error)
	GetCategoryByBookId(id uint64) ([]dao.Category, error)
	SearchBook(keys [2]string) ([]dto.SearchFilterBookDTO, error)
	FilterBook(keys []string) ([]dto.SearchFilterBookDTO, error)
}

func NewBookService(params bookServiceParams) BookService {
	return &params
}

func (u bookServiceParams) GetHomeData(id_user uint) (dto.HomeDTO, error) {
	res := dto.HomeDTO{}
	myCollection := dto.MyCollectionDTO{}
	ownedIdBooks := []int{}

	categories_dao, err := u.CategoryRepo.GetAllCategories()
	collections, err2 := u.CollectionRepo.GetCollectionByUserId(uint64(id_user))

	// checking if there is an error
	if err != nil {
		return res, err
	} else {
		// get my collection
		if err2 == nil {
			myCollection = dto.NewMyCollectionDTO(collections, id_user)

			for idx, item := range myCollection.Collections {
				ownedIdBooks = append(ownedIdBooks, int(item.BookID))
				myCollection.Collections[idx].Progress, _ = u.CollectionService.GetProgress(id_user, item.BookID)
			}
		}

		// get all categories (in dto)
		categories := []dto.CategoryDTO{}
		bookMapping := dto.BookMapping{}

		for _, category := range categories_dao {
			categories = append(categories,
				dto.CategoryDTO{
					CategoryName: category.CategoryName,
					ID:           category.ID,
				},
			)

			// get all books of category ID
			books, err1 := u.BookRepo.GetBooksByCategoryId(uint64(category.ID))

			if err1 == nil {
				for _, book := range books {
					categories := []string{}
					categories_resp, err2 := u.CategoryRepo.GetCategoryByBookId(uint64(book.ID))

					if err2 == nil {
						for _, category := range categories_resp {
							categories = append(categories, category.CategoryName)
						}

						// checking if book is owned and get progress
						owned := false
						progress := 0
						for _, id := range ownedIdBooks {
							if id == int(book.ID) {
								owned = true
								progress, _ = u.CollectionService.GetProgress(id_user, book.ID)
							}
						}

						bookMapping[category.CategoryName] = append(
							bookMapping[category.CategoryName], dto.BookDTO{
								Title:       book.Title,
								Price:       book.Price,
								Author:      book.Author,
								Description: book.Description,
								Cover:       book.Cover,
								Content:     book.Content,
								ID:          book.ID,
								IsOwned:     &owned,
								Progress:    &progress,
								Category:    categories,
							},
						)
					}
				}
			}
			res = dto.HomeDTO{
				MyItems:    myCollection.Collections,
				Categories: categories,
				Books:      bookMapping,
			}
		}
		return res, nil
	}
}

func (u *bookServiceParams) GetBook(id_user uint64, id_book uint64) (dto.BookDTO, error) {
	book, err := u.BookRepo.GetBook(id_book)

	res := dto.BookDTO{}
	if err == nil {
		categories := []string{}
		categories_resp, err2 := u.CategoryRepo.GetCategoryByBookId(id_book)

		if err2 == nil {
			for _, category := range categories_resp {
				categories = append(categories, category.CategoryName)
			}
			res = dto.NewBookDTO(book)
			res.Category = categories

			owned, _ := u.BookRepo.IsBookOwned(id_user, id_book)
			progress, _ := u.CollectionService.GetProgress(uint(id_user), uint(id_book))
			res.IsOwned = &owned
			res.Progress = &progress
		}
	} else {
		return res, err
	}
	return res, nil
}

func (u *bookServiceParams) GetCategoryByBookId(id uint64) ([]dao.Category, error) {
	return u.CategoryRepo.GetCategoryByBookId(id)
}

func (u *bookServiceParams) GetAllBook() ([]dto.BookDTO, error) {
	books, err := u.BookRepo.GetAll()
	if err != nil {
		return []dto.BookDTO{}, err
	}

	booksDTO := make([]dto.BookDTO, len(books))
	for i, v := range books {
		booksDTO[i] = dto.NewBookDTO(v)

		categories_resp, err2 := u.CategoryRepo.GetCategoryByBookId(uint64(v.ID))

		if err2 == nil {
			categories := []string{}
			for _, category := range categories_resp {
				categories = append(categories, category.CategoryName)
			}
			booksDTO[i].Category = categories
		}

	}
	return booksDTO, nil
}

func (u *bookServiceParams) DeleteBook(id uint64) error {
	return u.BookRepo.DeleteById(uint(id))
}

func (u *bookServiceParams) GetBooksByCategoryId(id uint64) ([]dao.Book, error) {
	return u.BookRepo.GetBooksByCategoryId(id)
}

func (u *bookServiceParams) CreateBook(book dto.CreateBookRequestDTO) (dao.Book, error) {
	coverUploaded, err := u.FileService.UploadFile(book.Cover)
	if err != nil {
		return dao.Book{}, err
	}

	contentUploaded, err := u.FileService.UploadFile(book.Content)

	if err != nil {
		return dao.Book{}, err
	}

	bookDAO := book.ToDAO(coverUploaded.FileCode, contentUploaded.FileCode)

	createdBook, err := u.BookRepo.CreateBook(bookDAO)
	if err != nil {
		return dao.Book{}, err
	}

	members := make([]dao.Member, len(book.Category))

	for i, c := range book.Category {
		members[i] = dao.Member{
			BookID:     createdBook.ID,
			CategoryID: c,
		}
	}

	_, err = u.MemberRepo.BulkAddMember(members)
	if err != nil {
		return dao.Book{}, err
	}

	return createdBook, nil
}

func (u *bookServiceParams) UpdateBook(id uint, book dto.UpdateBookRequestDTO) (dao.Book, error) {
	updateMap := utils.StructToMap(book)
	return u.BookRepo.UpdateBook(id, updateMap)
}

func (u *bookServiceParams) SearchBook(keys [2]string) ([]dto.SearchFilterBookDTO, error) {
	res := []dto.SearchFilterBookDTO{}
	myCollections := dto.MyCollectionDTO{}
	books, err := u.BookRepo.SearchBook(keys[0])
	if keys[1] != "" {
		id, _ := strconv.ParseUint(keys[1], 10, 32)
		myCollections, _ = u.CollectionService.GetCollectionByUserId(uint(id))
	}

	if err == nil {
		for _, book := range books {
			data := dto.SearchFilterBookDTO{}
			categories := []string{}
			bookCategories, err1 := u.CategoryRepo.GetCategoryByBookId(uint64(book.ID))

			if err1 == nil {
				for _, category := range bookCategories {
					categories = append(categories, category.CategoryName)
				}

				data = dto.NewSearchFilterBookDTO(book, categories, myCollections)
			}

			res = append(res, data)
		}
	} else {
		return res, err
	}
	if len(res) == 0 {
		return res, e.ErrBooksSearchNotFound
	}

	return res, nil
}

func (u *bookServiceParams) FilterBook(keys []string) ([]dto.SearchFilterBookDTO, error) {
	res := []dto.SearchFilterBookDTO{}
	myCollections := dto.MyCollectionDTO{}
	if keys[0] != "" {
		id, _ := strconv.ParseUint(keys[1], 10, 32)
		myCollections, _ = u.CollectionService.GetCollectionByUserId(uint(id))
	}

	queries := keys[1:]

	for _, query := range queries {
		category, err := u.CategoryRepo.GetCategoryByName(query)
		if err == nil {
			books, err1 := u.BookRepo.GetBooksByCategoryId(uint64(category.ID))

			if err1 == nil {
				for _, book := range books {
					data := dto.SearchFilterBookDTO{}
					categories := []string{}
					bookCategories, err2 := u.CategoryRepo.GetCategoryByBookId(uint64(book.ID))

					if err2 == nil {
						for _, category := range bookCategories {
							categories = append(categories, category.CategoryName)
						}

						data = dto.NewSearchFilterBookDTO(book, categories, myCollections)
					}
					exist := false
					for _, dt := range res {
						if data.ID == dt.ID {
							exist = true
						}
					}
					if !exist {
						res = append(res, data)
					}
				}
			}
		}
	}
	if len(res) == 0 {
		return res, e.ErrBooksFilterNotFound
	}

	return res, nil
}
