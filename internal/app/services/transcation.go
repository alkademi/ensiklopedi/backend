package services

import (
	"fmt"
	"strconv"

	"github.com/xendit/xendit-go"
	"github.com/xendit/xendit-go/ewallet"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/config"
	errors "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	transaction_status "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao/trxStatus"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/repo"
	"go.uber.org/fx"
)

type transactionServiceParams struct {
	fx.In

	Tr repo.TransactionRepo
	Br repo.BookRepo
	Ur repo.UserRepo
	Cr repo.CollectionRepo

	Cfg config.PaymentConfig
}

type TransactionService interface {
	BuyBook(userId, bookId uint, metadata map[string]interface{}) (dto.TransactionStatusDTO, error)
	XenditCallback(data dto.XenditCallbackDTO) (dto.TransactionStatusDTO, error)
}

func NewTransactionService(params transactionServiceParams) TransactionService {
	return &params
}

func (t transactionServiceParams) BuyBook(userId, bookId uint, metadata map[string]interface{}) (dto.TransactionStatusDTO, error) {
	collections, err := t.Cr.GetCollectionByUserId(uint64(userId))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	for _, c := range collections {
		if c.BookID == bookId {
			return dto.TransactionStatusDTO{}, errors.ErrAlreadyHaveBook
		}
	}
	book, err := t.Br.GetBook(uint64(bookId))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	user, err := t.Ur.GetUser(uint64(userId))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	if book.Price == 0 { // free
		err = t.purchaseFreeBook(userId, bookId)
		if err != nil {
			return dto.TransactionStatusDTO{}, err
		}
		return dto.TransactionStatusDTO{
			Amount: book.Price,
			Email:  user.Email,
			Book:   dto.NewBookDTO(book),
			Status: transaction_status.Completed,
			Metadata: map[string]interface{}{
				"message": "book is free",
			},
		}, nil
	} else {
		if metadata["payment_method"].(string) == "ID_OVO" {
			charge, err := t.createOVOPayment(book, user, metadata)
			if err != nil {
				return dto.TransactionStatusDTO{}, err
			}
			metadata["created"] = charge.Created

			return dto.TransactionStatusDTO{
				Amount:   book.Price,
				Email:    user.Email,
				Book:     dto.NewBookDTO(book),
				Status:   transaction_status.Pending,
				Metadata: metadata,
			}, nil
		} else {
			err := t.purcahseNonFreeBook(book, user)
			if err != nil {
				return dto.TransactionStatusDTO{}, err
			}
			return dto.TransactionStatusDTO{
				Amount:   book.Price,
				Email:    user.Email,
				Book:     dto.NewBookDTO(book),
				Status:   transaction_status.Completed,
				Metadata: metadata,
			}, nil
		}
	}
}

func (t transactionServiceParams) XenditCallback(data dto.XenditCallbackDTO) (dto.TransactionStatusDTO, error) {
	trxId, _ := strconv.ParseUint(data.Data.ReferenceID, 10, 32)
	transaction, err := t.Tr.GetTransactionByID(uint(trxId))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	book, err := t.Br.GetBook(uint64(transaction.BookID))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	user, err := t.Ur.GetUser(uint64(transaction.UserID))
	if err != nil {
		return dto.TransactionStatusDTO{}, err
	}
	if data.Data.Status == "SUCCEEDED" {
		transaction, err = t.Tr.UpdateTransaction(uint(trxId), transaction_status.Completed)
		if err != nil {
			return dto.TransactionStatusDTO{}, err
		}
		_, err := t.Cr.CreateCollection(dao.Collection{
			UserID: transaction.UserID,
			BookID: transaction.BookID,
		})
		if err != nil {
			return dto.TransactionStatusDTO{}, err
		}
		return dto.TransactionStatusDTO{
			Amount:   transaction.Amount,
			Email:    user.Email,
			Book:     dto.NewBookDTO(book),
			Status:   transaction_status.Completed,
			Metadata: data.Data.Metadata,
		}, nil
	}

	return dto.TransactionStatusDTO{}, errors.ErrPaymentGatewayFailed

}

func (t transactionServiceParams) purchaseFreeBook(userId, bookId uint) error {
	_, err := t.Tr.CreateTransaction(dao.Transaction{
		UserID: userId,
		BookID: bookId,
		Status: transaction_status.Completed,
		Amount: 0,
	})
	if err != nil {
		return err
	}
	_, err = t.Cr.CreateCollection(dao.Collection{UserID: userId, BookID: bookId})
	return err
}

func (t transactionServiceParams) purcahseNonFreeBook(book dao.Book, user dao.User) error {
	if user.Balance < float64(book.Price) {
		return errors.ErrBalanceNotEnough
	}
	_, err := t.Tr.CreateTransaction(dao.Transaction{
		UserID: user.ID,
		BookID: book.ID,
		Status: transaction_status.Completed,
	})

	if err != nil {
		return err
	}
	_, err = t.Cr.CreateCollection(dao.Collection{UserID: user.ID, BookID: book.ID})
	if err != nil {
		return err
	}
	_, err = t.Ur.UpdateUser(user.ID, map[string]interface{}{
		"balance": (user.Balance - float64(book.Price)),
	})

	return err
}

func (t transactionServiceParams) createOVOPayment(book dao.Book, user dao.User, metadata map[string]interface{}) (*xendit.EWalletCharge, error) {
	trx, err := t.Tr.CreateTransaction(dao.Transaction{
		UserID: user.ID,
		BookID: book.ID,
		Status: transaction_status.Pending,
	})
	if err != nil {
		return nil, err
	}
	if _, ok := metadata["mobile_number"]; !ok {
		return nil, errors.ErrPhoneNumberNotProvided
	}

	data := ewallet.CreateEWalletChargeParams{
		ReferenceID:    fmt.Sprint(trx.ID),
		Currency:       "IDR",
		Amount:         float64(book.Price),
		CheckoutMethod: "ONE_TIME_PAYMENT",
		ChannelCode:    "ID_OVO",
		ChannelProperties: map[string]string{
			"mobile_number": metadata["mobile_number"].(string),
		},
	}
	charge, chargeErr := ewallet.CreateEWalletCharge(&data)
	if chargeErr == nil {
		return charge, nil
	}
	return charge, errors.New(chargeErr.Error())
}
