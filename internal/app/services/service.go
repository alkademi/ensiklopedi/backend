package services

import (
	"go.uber.org/fx"
)

// Module exports services present
var Module = fx.Options(
	fx.Provide(NewUserService),
	fx.Provide(NewBookService),
	fx.Provide(NewFileService),
	fx.Provide(NewCollectionService),
	fx.Provide(NewCategoryService),
	fx.Provide(NewTransactionService),
)
