package middlewares

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"go.uber.org/fx"

	_ "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/docs"

	echoSwagger "github.com/swaggo/echo-swagger"
)

type swaggerMiddlewareParams struct {
	fx.In

	Handler lib.HTTPServer
}

type SwaggerMiddleware Middleware

func NewSwaggerMiddleware(params swaggerMiddlewareParams) SwaggerMiddleware {
	return &params
}

func (p swaggerMiddlewareParams) Setup() {
	p.Handler.Engine().GET("/docs/*", echoSwagger.WrapHandler)
}
