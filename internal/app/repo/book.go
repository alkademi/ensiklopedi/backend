package repo

import (
	"errors"
	"fmt"

	e "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"go.uber.org/fx"
	"gorm.io/gorm"
)

type BookRepo interface {
	IsBookOwned(id_user uint64, id_book uint64) (bool, error)
	GetBook(id uint64) (dao.Book, error)
	GetBooksByCategoryId(id uint64) ([]dao.Book, error)
	CreateBook(book dao.Book) (dao.Book, error)
	SearchBook(key string) ([]dao.Book, error)
	UpdateBook(id uint, updateMap map[string]interface{}) (dao.Book, error)
	GetAll() ([]dao.Book, error)
	DeleteById(id uint) error
}
type bookRepoParams struct {
	fx.In

	lib.Database
}

func NewBookRepo(params bookRepoParams) BookRepo {
	return &params
}

func (p *bookRepoParams) IsBookOwned(id_user uint64, id_book uint64) (bool, error) {
	var count int64
	collection := dao.Collection{}

	if err := p.Db.Where("id_user = ? AND id_book = ?", id_user, id_book).Find(&collection).Count(&count).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return false, e.ErrBookNotFound
		}

		return false, err
	}

	return count > 0, nil
}

func (p *bookRepoParams) GetAll() ([]dao.Book, error) {
	books := []dao.Book{}

	if err := p.Db.Find(&books).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return []dao.Book{}, e.ErrBookNotFound
		}

		return []dao.Book{}, err
	}
	return books, nil
}

func (p *bookRepoParams) GetBook(id uint64) (dao.Book, error) {
	book := dao.Book{}

	if err := p.Db.First(&book, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return dao.Book{}, e.ErrBookNotFound
		}

		return dao.Book{}, err
	}
	return book, nil
}

func (p *bookRepoParams) GetBooksByCategoryId(id uint64) ([]dao.Book, error) {
	books := []dao.Book{}

	if err := p.Db.Joins("JOIN members ON members.id_book = books.id").Joins("JOIN categories ON categories.id = members.id_category").Where("members.id_category = ?", id).Find(&books).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return books, e.ErrBooksFilterNotFound
		}

		return books, err
	}
	return books, nil
}

func (p *bookRepoParams) CreateBook(book dao.Book) (dao.Book, error) {

	if err := p.Db.Create(&book).Error; err != nil {
		return dao.Book{}, err
	}

	return book, nil
}

func (p *bookRepoParams) SearchBook(key string) ([]dao.Book, error) {
	books := []dao.Book{}

	if err := p.Db.Where("title LIKE ?", "%"+key+"%").Or("author LIKE ?", "%"+key+"%").Or("description LIKE ?", "%"+key+"%").Or("publisher LIKE ?", "%"+key+"%").Or("isbn LIKE ?", "%"+key+"%").Find(&books).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return books, e.ErrBooksSearchNotFound
		}
		fmt.Println("lolos")
		return books, err
	}

	return books, nil
}
func (p *bookRepoParams) UpdateBook(id uint, updateMap map[string]interface{}) (dao.Book, error) {

	if err := p.Db.Model(&dao.Book{}).Where("id = ?", id).Updates(updateMap).Error; err != nil {
		return dao.Book{}, err
	}
	return p.GetBook(uint64(id))
}

func (p *bookRepoParams) DeleteById(id uint) error {
	if err := p.Db.Unscoped().Delete(&dao.Book{}, id).Error; err != nil {
		return err
	}
	return nil
}
