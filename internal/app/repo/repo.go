package repo

import (
	"go.uber.org/fx"
)

// Module exported for initializing application
var Module = fx.Options(
	fx.Provide(NewUserRepo),
	fx.Provide(NewBookRepo),
	fx.Provide(NewCategoryRepo),
	fx.Provide(NewFileRepo),
	fx.Provide(NewCollectionRepo),
	fx.Provide(NewMemberRepo),
	fx.Provide(NewTransactionRepo),
)
