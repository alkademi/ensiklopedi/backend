package repo

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"go.uber.org/fx"
)

type MemberRepo interface {
	BulkAddMember(members []dao.Member) ([]dao.Member, error)
	DeleteByBookId(bookId []uint)
}
type memberRepoParams struct {
	fx.In

	lib.Database
}

func NewMemberRepo(params memberRepoParams) MemberRepo {
	return &params
}

func (p *memberRepoParams) BulkAddMember(members []dao.Member) ([]dao.Member, error) {
	if err := p.Db.Create(&members).Error; err != nil {
		return []dao.Member{}, err
	}

	return members, nil
}

func (p *memberRepoParams) DeleteByBookId(bookId []uint) {
	p.Db.Delete(&dao.Member{}, bookId)
}
