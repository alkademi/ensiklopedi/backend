// Code generated by mockery v2.10.4. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	dao "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"

	transaction_status "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao/trxStatus"
)

// TransactionRepo is an autogenerated mock type for the TransactionRepo type
type TransactionRepo struct {
	mock.Mock
}

// CreateTransaction provides a mock function with given fields: _a0
func (_m *TransactionRepo) CreateTransaction(_a0 dao.Transaction) (dao.Transaction, error) {
	ret := _m.Called(_a0)

	var r0 dao.Transaction
	if rf, ok := ret.Get(0).(func(dao.Transaction) dao.Transaction); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(dao.Transaction)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(dao.Transaction) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTransactionByID provides a mock function with given fields: trxId
func (_m *TransactionRepo) GetTransactionByID(trxId uint) (dao.Transaction, error) {
	ret := _m.Called(trxId)

	var r0 dao.Transaction
	if rf, ok := ret.Get(0).(func(uint) dao.Transaction); ok {
		r0 = rf(trxId)
	} else {
		r0 = ret.Get(0).(dao.Transaction)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint) error); ok {
		r1 = rf(trxId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetTransactionByUIDBID provides a mock function with given fields: userId, bookId
func (_m *TransactionRepo) GetTransactionByUIDBID(userId uint, bookId uint) (dao.Transaction, error) {
	ret := _m.Called(userId, bookId)

	var r0 dao.Transaction
	if rf, ok := ret.Get(0).(func(uint, uint) dao.Transaction); ok {
		r0 = rf(userId, bookId)
	} else {
		r0 = ret.Get(0).(dao.Transaction)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint, uint) error); ok {
		r1 = rf(userId, bookId)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateTransaction provides a mock function with given fields: trxId, status
func (_m *TransactionRepo) UpdateTransaction(trxId uint, status transaction_status.TransactionStatus) (dao.Transaction, error) {
	ret := _m.Called(trxId, status)

	var r0 dao.Transaction
	if rf, ok := ret.Get(0).(func(uint, transaction_status.TransactionStatus) dao.Transaction); ok {
		r0 = rf(trxId, status)
	} else {
		r0 = ret.Get(0).(dao.Transaction)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uint, transaction_status.TransactionStatus) error); ok {
		r1 = rf(trxId, status)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
