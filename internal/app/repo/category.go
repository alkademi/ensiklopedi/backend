package repo

import (
	"errors"

	e "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
	"go.uber.org/fx"
	"gorm.io/gorm"
)

type CategoryRepo interface {
	GetAllCategories() ([]dao.Category, error)
	GetCategoryByBookId(id uint64) ([]dao.Category, error)
	GetCategoryByName(name string) (dao.Category, error)
	CreateCategory(category dao.Category) (dao.Category, error)
	GetCategory(id uint64) (dao.Category, error)
	UpdateCategory(newCategory dto.CategoryDTO) (dao.Category, error)
	DeleteCategory(categoryId dto.DeleteCategoryRequestDTO) error
}
type categoryRepoParams struct {
	fx.In

	lib.Database
}

func NewCategoryRepo(params categoryRepoParams) CategoryRepo {
	return &params
}

func (p *categoryRepoParams) GetAllCategories() ([]dao.Category, error) {
	categories := []dao.Category{}

	if err := p.Db.Find(&categories).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return categories, e.ErrCategoriesNotFound
		}

		return categories, err
	}
	return categories, nil
}

func (p *categoryRepoParams) GetCategoryByBookId(id uint64) ([]dao.Category, error) {
	categories := []dao.Category{}

	if err := p.Db.Joins("JOIN members ON members.id_category = categories.id").Joins("JOIN books ON books.id = members.id_book").Where("books.id = ?", id).Select("category_name").Find(&categories).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return categories, e.ErrCategoriesNotFound
		}

		return categories, err
	}

	return categories, nil
}

func (p *categoryRepoParams) GetCategoryByName(name string) (dao.Category, error) {
	category := dao.Category{}

	if err := p.Db.Where("category_name = ?", name).First(&category).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return category, e.ErrCategoriesNotFound
		}

		return category, err
	}

	return category, nil
}

func (p *categoryRepoParams) CreateCategory(category dao.Category) (dao.Category, error) {
	if err := p.Db.Create(&category).Error; err != nil {
		return dao.Category{}, e.ErrCreateCategory
	}

	return category, nil
}

func (p *categoryRepoParams) GetCategory(id uint64) (dao.Category, error) {
	category := dao.Category{}

	if err := p.Db.First(&category, id).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return dao.Category{}, e.ErrCategoryNotFound
		}

		return dao.Category{}, err
	}
	return category, nil
}

func (p *categoryRepoParams) UpdateCategory(newCategory dto.CategoryDTO) (dao.Category, error) {
	category := dao.Category{}

	if err := p.Db.Model(&category).Where("id_category = ?", newCategory.ID).Update("category_name", newCategory.CategoryName).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return category, e.ErrCategoryNotFound
		}
		return category, e.ErrUpdateCategory
	}
	return category, nil
}

func (p *categoryRepoParams) DeleteCategory(categoryId dto.DeleteCategoryRequestDTO) error {
	category := dao.Category{}

	if err := p.Db.Delete(&category, categoryId.ID).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return e.ErrCategoryNotFound
		}

		return e.ErrDeleteCategory
	}

	return nil
}
