package repo

import (
	"errors"

	e "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"go.uber.org/fx"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type CollectionRepo interface {
	GetAllCollection() ([]dao.Collection, error)
	GetCollectionByUserId(id uint64) ([]dao.Collection, error)
	CreateCollection(c dao.Collection) (dao.Collection, error)
	TrackProgress(userID uint, bookID uint, progress int) (dao.Collection, error)
}

type collectionRepoParams struct {
	fx.In

	lib.Database
}

func NewCollectionRepo(params collectionRepoParams) CollectionRepo {
	return &params
}

func (p *collectionRepoParams) GetAllCollection() ([]dao.Collection, error) {
	collections := []dao.Collection{}

	if err := p.Db.Find(&collections).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return collections, e.ErrEmptyCollection
		}

		return collections, err
	}
	return collections, nil
}

func (p *collectionRepoParams) GetCollectionByUserId(id uint64) ([]dao.Collection, error) {
	collections := []dao.Collection{}

	if err := p.Db.Preload(clause.Associations).Where("id_user = ?", id).Find(&collections).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return collections, e.ErrEmptyCollection
		}

		return collections, err
	}

	return collections, nil
}

func (p *collectionRepoParams) CreateCollection(c dao.Collection) (dao.Collection, error) {
	if err := p.Db.Create(&c).Error; err != nil {
		return dao.Collection{}, err
	}

	return c, nil
}
func (p *collectionRepoParams) TrackProgress(userID uint, bookID uint, progress int) (dao.Collection, error) {
	target := dao.Collection{}
	if err := p.Db.Model(&target).Clauses(clause.Returning{}).Where("id_user = ? AND id_book = ?", userID, bookID).Update("progress", progress).Error; err != nil {
		return dao.Collection{}, e.ErrCollectionNotFound
	}
	return target, nil
}
