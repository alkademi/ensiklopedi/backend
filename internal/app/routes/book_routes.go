package routes

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/controllers"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"go.uber.org/fx"
)

// PolicyRouteParams defines route for policy
type policyRouteParamsBook struct {
	fx.In

	Controller controllers.BookController
	Handler    lib.HTTPServer
}

// PolicyRoute defines route for policy
type BookRoute Route

// NewPolicyRoutes creates new instance of PolicyRouteImpl
func NewBookRoutes(pr policyRouteParamsBook) BookRoute {
	return &pr
}

// Setup PolicyRouteImpl
func (a *policyRouteParamsBook) Setup() {

	r := a.Handler.RouterV1().Group("/book")
	r.GET("", a.Controller.GetAllBook)
	r.GET("/:id", a.Controller.GetBook)
	r.GET("/search/:search", a.Controller.SearchBook)
	r.GET("/category/:category", a.Controller.FilterBook)
	r.DELETE("/:id", a.Controller.DeleteBook)
	r.PUT("/:id", a.Controller.UpdateBook)
	r.POST("/", a.Controller.CreateBook)

	r1 := a.Handler.RouterV1().Group("/home")
	r1.GET("/", a.Controller.GetHomeData)
}
