package routes

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/controllers"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"go.uber.org/fx"
)

// PolicyRouteParams defines route for policy
type policyRouteParamsCollection struct {
	fx.In

	Controller controllers.CollectionController
	Handler    lib.HTTPServer
}

// PolicyRoute defines route for policy
type CollectionRoute Route

// NewPolicyRoutes creates new instance of PolicyRouteImpl
func NewCollectionRoutes(pr policyRouteParamsCollection) CollectionRoute {
	return &pr
}

// Setup PolicyRouteImpl
func (a *policyRouteParamsCollection) Setup() {

	r := a.Handler.RouterV1().Group("/collection")
	r.GET("/", a.Controller.GetCollectionData)
	r.PATCH("/track/:bookId/:progress", a.Controller.TrackProgress)
}
