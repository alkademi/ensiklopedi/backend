package routes

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/controllers"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"go.uber.org/fx"
)

// PolicyRouteParams defines route for policy
type transactionRouteParams struct {
	fx.In

	Controller controllers.TransactionController
	Handler    lib.HTTPServer
}

// PolicyRoute defines route for policy
type TransactionRoute Route

// NewPolicyRoutes creates new instance of PolicyRouteImpl
func NewTransactionRoutes(pr transactionRouteParams) TransactionRoute {
	return &pr
}

// Setup PolicyRouteImpl
func (a *transactionRouteParams) Setup() {

	r := a.Handler.RouterV1().Group("/transaction")
	r.POST("/buy", a.Controller.PurchaseBook)
	r.POST("/xendit/cb", a.Controller.XenditCallback)

}
