package routes

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/controllers"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"go.uber.org/fx"
)

// PolicyRouteParams defines route for policy
type policyRouteParamsCategory struct {
	fx.In

	Controller controllers.CategoryController
	Handler    lib.HTTPServer
}

// PolicyRoute defines route for policy
type CategoryRoute Route

// NewPolicyRoutes creates new instance of PolicyRouteImpl
func NewCategoryRoutes(pr policyRouteParamsCategory) CategoryRoute {
	return &pr
}

// Setup PolicyRouteImpl
func (a *policyRouteParamsCategory) Setup() {

	r := a.Handler.RouterV1().Group("/category")
	r.GET("/", a.Controller.GetAll)
	r.POST("/create", a.Controller.CreateCategory)
	r.GET("/:id", a.Controller.GetCategory)
	r.PUT("/update", a.Controller.UpdateCategory)
	r.DELETE("/delete", a.Controller.DeleteCategory)
}
