package routes

import (
	"go.uber.org/fx"
)

// Module exports dependency to container
var Module = fx.Options(
	fx.Provide(NewUserRoutes),
	fx.Provide(NewBookRoutes),
	fx.Provide(NewFileRoutes),
	fx.Provide(NewRoutes),
	fx.Provide(NewCollectionRoutes),
	fx.Provide(NewCategoryRoutes),
	fx.Provide(NewTransactionRoutes),
)

// Routes contains multiple routes
type Routes []Route

// Route interface
//go:generate mockery --name=Route --case underscore --inpackage
type Route interface {
	Setup()
}

// NewRoutes sets up routes
func NewRoutes(
	userRoutes UserRoute,
	bookRoutes BookRoute,
	fileRoutes FileRoute,
	collectionRoutes CollectionRoute,
	categoryRoutes CategoryRoute,
	transactionRoutes TransactionRoute,
) Routes {
	return Routes{
		userRoutes,
		bookRoutes,
		fileRoutes,
		collectionRoutes,
		categoryRoutes,
		transactionRoutes,
	}
}

// Setup all the route
func (a Routes) Setup() {
	for _, route := range a {
		route.Setup()
	}
}
