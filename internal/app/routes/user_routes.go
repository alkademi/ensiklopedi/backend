package routes

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/constants"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/controllers"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/middlewares"
	"go.uber.org/fx"
)

// PolicyRouteParams defines route for policy
type userRouteParams struct {
	fx.In

	Controller controllers.UserController
	Handler    lib.HTTPServer
	Middleware middlewares.AuthMiddleware
}

// PolicyRoute defines route for policy
type UserRoute Route

// NewPolicyRoutes creates new instance of PolicyRouteImpl
func NewUserRoutes(pr userRouteParams) UserRoute {
	return &pr
}

// Setup PolicyRouteImpl
func (a *userRouteParams) Setup() {

	r := a.Handler.RouterV1().Group("/user")
	r.POST("/register", a.Controller.CreateUser)
	r.GET("/:id", a.Controller.GetUser, a.Middleware.Setup(constants.PermissionNonAdmin))
	r.POST("/", a.Controller.CreateUser)
	r.POST("/login", a.Controller.Login)
}
