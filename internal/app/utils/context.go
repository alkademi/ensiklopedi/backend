package utils

import (
	"encoding/json"
	"strings"

	"github.com/labstack/echo/v4"
	errors "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/error"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/lib"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dto"
)

func ExtractToken(c echo.Context) (string, error) {
	authorizationHeader := c.Request().Header.Get("Authorization")

	if authorizationHeader == "" {
		return "", errors.ErrMissingAuthorization
	}

	if !strings.HasPrefix(strings.ToLower(authorizationHeader), "bearer") {
		return "", errors.ErrWrongAuthorization
	}

	splitted := strings.Split(authorizationHeader, " ")

	if len(splitted) != 2 {
		return "", errors.ErrWrongAuthorization
	}

	token := splitted[1]

	return token, nil
}

func GetUserFromToken(token string, jwt lib.JWT) (dto.UserDTO, error) {
	userMap, err := jwt.Decode(token)
	if err != nil {
		return dto.UserDTO{}, err
	}

	jsonString, _ := json.Marshal(userMap)
	var user dto.UserDTO
	json.Unmarshal(jsonString, &user)
	if user.ID == 0 {
		return dto.UserDTO{}, errors.ErrDecodingJWT
	}

	return user, nil
}
