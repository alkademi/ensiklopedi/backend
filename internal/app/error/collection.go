package errors

import (
	"errors"
	"net/http"
)

const ()

var (
	ErrCollectionNotFound = errors.New("collection not found")
	ErrEmptyCollection    = errors.New("empty collection")

	collectionErrorMapper ErrorMapper = ErrorMapper{
		ErrCollectionNotFound: http.StatusNotFound,
		ErrEmptyCollection:    http.StatusNotFound,
	}
)
