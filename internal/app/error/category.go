package errors

import (
	"errors"
	"net/http"
)

const ()

var (
	ErrCategoryNotFound = errors.New("there is no categories with that id")
	ErrCreateCategory   = errors.New("there is category with that name")
	ErrUpdateCategory   = errors.New("cannot update category with that id")
	ErrDeleteCategory   = errors.New("cannot delete category with that id")

	categoryErrorMapper ErrorMapper = ErrorMapper{
		ErrCategoriesNotFound: http.StatusNotFound,
		ErrCreateCategory:     http.StatusBadRequest,
		ErrUpdateCategory:     http.StatusBadRequest,
		ErrDeleteCategory:     http.StatusBadRequest,
	}
)
