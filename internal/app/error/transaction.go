package errors

import (
	"errors"
	"net/http"
)

var (
	ErrPhoneNumberNotProvided = errors.New("no phone number")
	ErrBalanceNotEnough       = errors.New("balance not enough")
	ErrTransactionNotFound    = errors.New("transaction not found")
	ErrPaymentGatewayFailed   = errors.New("payment gateway failed")
	ErrAlreadyHaveBook        = errors.New("already have book")

	transactionErrorMapper ErrorMapper = ErrorMapper{
		ErrPhoneNumberNotProvided: http.StatusBadRequest,
		ErrBalanceNotEnough:       http.StatusBadRequest,
		ErrTransactionNotFound:    http.StatusNotFound,
		ErrPaymentGatewayFailed:   http.StatusBadRequest,
		ErrAlreadyHaveBook:        http.StatusBadRequest,
	}
)
