package errors

import (
	"errors"
	"net/http"
)

const ()

var (
	ErrCategoriesNotFound  = errors.New("there is no categories in database")
	ErrBookNotFound        = errors.New("book with that id is not found")
	ErrBooksFilterNotFound = errors.New("book with that category is not found")
	ErrBooksSearchNotFound = errors.New("book not found")

	bookErrorMapper ErrorMapper = ErrorMapper{
		ErrCategoriesNotFound:  http.StatusNotFound,
		ErrBookNotFound:        http.StatusNotFound,
		ErrBooksFilterNotFound: http.StatusNotFound,
		ErrBooksSearchNotFound: http.StatusNotFound,
	}
)
