package lib

import (
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/config"
	"gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/internal/app/model/dao"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	Db *gorm.DB
}

func NewDatabase(cfg config.DatabaseConfig) Database {
	db, err := gorm.Open(postgres.Open(cfg.DSN()), &gorm.Config{})
	if err != nil {
		panic("error connecting to database")
	}
	db.AutoMigrate(&dao.User{})
	db.AutoMigrate(&dao.Book{})
	db.AutoMigrate(&dao.Category{})
	db.AutoMigrate(&dao.Member{})
	db.AutoMigrate(&dao.Transaction{})
	db.AutoMigrate(&dao.Collection{})
	db.AutoMigrate(&dao.File{})
	return Database{
		Db: db,
	}
}
