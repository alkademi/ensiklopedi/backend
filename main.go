package main

import "gitlab.informatika.org/rahmahkn/if3250_2022_03_ensiklo/cmd/rootcmd"

// @title Ensiklopedi API
// @version 1.0
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8080
// @BasePath /
func main() {
	rootcmd.Execute()
}
